using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Group : MonoBehaviour
{

    public TMP_InputField p1;
    public TMP_InputField p2;
    

    public void SetInfo(string p1Mon, string p2mon)
    {
        p1.text = p1Mon;
        p2.text = p2mon;
    }

    public string GetP1()
    {
        return p1.text;
    }

    public string GetP2()
    {
        return p2.text;
    }
}
