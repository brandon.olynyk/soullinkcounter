# SoulLinkCounter

Uses Unity 2021.2.9f1.

A tool made in Unity to make remembering who's linked to who in a Pokemon Soul Link easier.

NOTE: There are certain sound files I had to remove because I don't want to use sounds ripped from another game without permission. Even though these files are removed, the app should still work out of the box.
