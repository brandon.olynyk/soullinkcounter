using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SaveLoad : MonoBehaviour
{
    public TMP_InputField p1name;
    public TMP_InputField p2name;
    public TMP_InputField title;
    public GroupGroup groupGroup;
    
    void Start()
    {
        Load();
    }

    public void Save()
    {
        PlayerPrefs.DeleteAll();

        //save panel count

        //save all info
        //name data
        PlayerPrefs.SetString("p1name", p1name.text);
        PlayerPrefs.SetString("p2name", p2name.text);
        PlayerPrefs.SetString("runTitle", title.text);
        //pokemon data

        for (int i = 0; i < groupGroup.groups.Count; i++)
        {
            Group groupScript = groupGroup.groups[i].GetComponent<Group>();
            PlayerPrefs.SetString("group" + i + "pokemon1", groupScript.p1.text);
            PlayerPrefs.SetString("group" + i + "pokemon2", groupScript.p2.text);
        }
        print("Groups saved: " + groupGroup.groups.Count);
        PlayerPrefs.SetInt("groupCount", groupGroup.groups.Count);

        PlayerPrefs.Save();
    }

    public void Load()
    {
        //load namedata
        p1name.text = PlayerPrefs.GetString("p1name");
        p2name.text = PlayerPrefs.GetString("p2name");
        title.text = PlayerPrefs.GetString("runTitle");
        //clear all panels
        groupGroup.Clear();
        //create needed # of panels
        for (int i = 0; i < PlayerPrefs.GetInt("groupCount"); i++)
        {
            groupGroup.SpawnGroup();
        }
        //fill panels

        for (int i = 0; i < groupGroup.groups.Count; i++)
        {
            groupGroup.groups[i].GetComponent<Group>().SetInfo(PlayerPrefs.GetString("group" + i + "pokemon1"), PlayerPrefs.GetString("group" + i + "pokemon2"));
        }
    }
}
