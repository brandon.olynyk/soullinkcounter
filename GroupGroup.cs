using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupGroup : MonoBehaviour
{
    public GameObject groupPrefab;
    public List<GameObject> groups = new List<GameObject>();

    public void SpawnGroup()
    {

        GameObject obj = Instantiate(groupPrefab, transform.position, Quaternion.identity);
        obj.transform.SetParent(transform);
        obj.transform.localScale = new Vector3(1, 1, 1);
        groups.Add(obj);
    }

    public void RemoveGroup()
    {
        if(transform.childCount > 0)
        {
            print("removal");
            GameObject obj = transform.GetChild(transform.childCount - 1).gameObject;
            groups.Remove(obj);
            Destroy(obj);
        }
       
    }

    public void Clear()
    {
        print("Count: " + groups.Count);
        //clear children
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        groups.Clear();
    }
}
