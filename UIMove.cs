using UnityEngine;
public class UIMove : MonoBehaviour
{
    public float speed;
    void Update()
    { 
        transform.Translate(Vector2.right * Input.mouseScrollDelta.y * speed);
    }
}
